resource "aws_route_table" "test_public_rt" {
  vpc_id = "${aws_vpc.test_app.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.test-igw.id}"
  }
  tags {
    Name = "public_rt"
  }
}
