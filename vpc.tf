resource "aws_vpc" "test_app" {
  cidr_block = "${var.test_app_vpc_cidr}"
  instance_tenancy = "default"
  tags {
    Name = "test-vpc"
  }
}

resource "aws_internet_gateway" "test-igw" {
  vpc_id = "${aws_vpc.test_app.id}"
  tags {
    Name = "test-igw"
  }
}
